package main

import (
	"context"
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"net/http"
	"os"
	"strings"
	"time"
	cloudkms "cloud.google.com/go/kms/apiv1"
	kmspb "google.golang.org/genproto/googleapis/cloud/kms/v1"
)

// Create the JWT key used to create the signature
var jwtKey = []byte(os.Getenv("JWT_SECRET_KEY"))

// access token password for authorising the request
var accessKey = os.Getenv("SIGN_API_KEY") // "6b86b273ff34fce19d6b804eff5a3f5747ada4eaa22f1d49c01e52ddb7875b4b"

func main() {
	if len(accessKey) <= 0 {
		fmt.Println(fmt.Errorf("SIGN_API_KEY is not present. run \"export SIGN_API_KEY=<API_KEY>\""))
		return
	}
	if len(jwtKey) <= 0 {
		fmt.Println(fmt.Errorf("JWT_SECRET_KEY is not present. run \"export JWT_SECRET_KEY=<SECRET_KEY>\""))
		return
	}
	http.HandleFunc("/getToken", getToken)
	http.HandleFunc("/sign/", SignMessage)
	http.ListenAndServe(":8080", nil)
}

// Create a struct to read the username and password from the request body
type Credentials struct {
	AccessKey string `json:"access_key"`
}

type Claims struct {
	jwt.StandardClaims
}

// Create the Signin handler
func getToken(w http.ResponseWriter, r *http.Request) {

	fmt.Println("entering signin")
	var creds Credentials

	err := json.NewDecoder(r.Body).Decode(&creds)
	if err != nil {
		// If the structure of the body is wrong, return an HTTP error
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(w, "{\"message\": \"No 'access_key' submitted\"}")
		return
	}
	fmt.Println(creds)

	// get sha256 of access key sent by user
	ParsedUserKey := sha256.Sum256([]byte(creds.AccessKey))

	// comparing hash of  access key with predefined key
	if accessKey != hex.EncodeToString(ParsedUserKey[:]) {
		w.WriteHeader(http.StatusUnauthorized)
		fmt.Fprintf(w, "{\"message\": \"Invalid access_key\"}")
		return
	}

	expirationTime := time.Now().Add(60 * time.Minute)
	issueTime := time.Now()
	// Create the JWT claims, which includes the username and expiry time
	claims := &Claims{
		StandardClaims: jwt.StandardClaims{
			// In JWT, the expiry time is expressed as unix milliseconds
			ExpiresAt: expirationTime.Unix(),
			Issuer:	"print2block-test",
			IssuedAt:	issueTime.Unix(),
		},
	}

	// Declare the token with the algorithm used for signing, and the claims
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	// Create the JWT string
	tokenString, err := token.SignedString(jwtKey)
	if err != nil {
		// If there is an error in creating the JWT return an internal server error
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, "{\"message\": \"Encountered problems in creating JWT\"}")
		return
	}
	fmt.Println(tokenString)
	w.Header().Add("Content-Type", "application/json")
	fmt.Fprintf(w, "{\"access-token\": \"%s\"}", tokenString)
}

func SignMessage(w http.ResponseWriter, r *http.Request) {
	if r.Method != "GET" {
		w.WriteHeader(http.StatusNotFound)
		return
	}
	w.Header().Add("Content-Type", "application/json")
	// Get token from the header
	tokenHeader := r.Header.Get("Authorization")

	// Token is missing, returns with error code 403 Unauthorized
	if tokenHeader == "" {
		w.WriteHeader(http.StatusForbidden)
		fmt.Fprintf(w, "{\"message\": \"No Token in present in the Header\"}")
		return
	}

	// The token normally comes in format `Bearer {token-body}`, we check if the retrieved token matched this requirement
	splitted := strings.Split(tokenHeader, " ")
	if len(splitted) != 2 {
		w.WriteHeader(http.StatusForbidden)
		fmt.Fprintf(w, "{\"message\": \"Token is not in the format `Bearer {token-body}`\"}")
		return
	}

	tokenString := splitted[1]

	claims := &Claims{}

	token, err := jwt.ParseWithClaims(tokenString, claims, func(token *jwt.Token) (interface{}, error) {
		return jwtKey, nil
	})

	if err != nil {
		if err == jwt.ErrSignatureInvalid {
			w.WriteHeader(http.StatusUnauthorized)
			fmt.Fprintf(w, "{\"message\": \"Invalid signature in token\"}")
			return
		}
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(w, "{\"message\": \"Bad request\"}")
		return
	}
	if !token.Valid {
		w.WriteHeader(http.StatusUnauthorized)
		fmt.Fprintf(w, "{\"message\": \"Token not valid\"}")
		return
	}

	message := r.URL.Path[6:]
	msgLen := len(message)

	if msgLen <= 0 {
		fmt.Println("hash is empty")
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(w, "{\"message\": \"no hash is present to sign\"}")
		return
	}

	signature, err := signAsymmetric(message)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, "{\"message\": \"Encountered issues during signing\"}")
		return
	}
	fmt.Fprintf(w, "{\"signature\": \"%s\"}", signature)
}

func signAsymmetric(message string) (interface{}, error) {
	name := "projects/p2b-network/locations/asia-south1/keyRings/Print2block/cryptoKeys/test-0/cryptoKeyVersions/1"
	ctx := context.Background()
	client, err := cloudkms.NewKeyManagementClient(ctx)
	if err != nil {
		return nil, fmt.Errorf("cloudkms.NewKeyManagementClient: %v", err)
	}

	// Find the digest of the message.
	digest := sha256.New()
	digest.Write([]byte(message))

	// Build the signing request.
	req := &kmspb.AsymmetricSignRequest{
		Name: name,
		Digest: &kmspb.Digest{
			Digest: &kmspb.Digest_Sha256{
				Sha256: digest.Sum(nil),
			},
		},
	}

	// Call the API.
	response, err := client.AsymmetricSign(ctx, req)
	if err != nil {
		return nil, fmt.Errorf("AsymmetricSign: %v", err)
	}

	// Signature is base64 encoded.
	// fmt.Println(response.Signature)
	fmt.Println(hex.EncodeToString(response.Signature))

	return hex.EncodeToString(response.Signature), nil
}